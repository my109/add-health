* Add Health data loading, cleaning, and variable generation
* Provided by Duke University Population Research Institute

set maxvar 10000

cd "~/Downloads/Add Health/Stata"

use "add_health_wave_1_in_home.dta", clear

/* CONSTRUCTING RACE VARIABLE

Wave I in-home interview variables used to construct RACE.

H1GI4 Are you of Hispanic or Latino origin?
H1GI6A What is your race? white
H1GI6B What is your race? black or African American
H1GI6C What is your race? American Indian or Native American
H1GI6D What is your race? Asian or Pacific Islander
H1GI6E What is your race? other
*/

* Computing race (including Hispanic/Latino) from the various categories with one category only being prioritized, in the following order.
generate RACE1 = .
replace RACE1 = 1 if H1GI4 == 1
replace RACE1 = 2 if H1GI6B == 1 & RACE1 != 1
replace RACE1 = 3 if H1GI6D == 1 & RACE1 > 2
replace RACE1 = 4 if H1GI6C == 1 & RACE1 > 3
replace RACE1 = 5 if H1GI6E == 1 & RACE1 > 4
replace RACE1 = 6 if H1GI6A == 1 & RACE1 > 5

/* Computing race ONLY (not including Hispanic/Latino) from the various categories with no prioritization and allowance for 2 or more races
White = 1
Black = 2
American Indian or Native American = 3
Asian or Pacific Islander = 4
Other = 5
2 or more races = 6
*/

generate RACE2 = .
replace RACE2 = 1 if H1GI6A == 1 & H1GI6B == 0 & H1GI6C == 0 & H1GI6D == 0 & H1GI6E == 0
replace RACE2 = 2 if H1GI6A == 0 & H1GI6B == 1 & H1GI6C == 0 & H1GI6D == 0 & H1GI6E == 0
replace RACE2 = 3 if H1GI6A == 0 & H1GI6B == 0 & H1GI6C == 1 & H1GI6D == 0 & H1GI6E == 0
replace RACE2 = 4 if H1GI6A == 0 & H1GI6B == 0 & H1GI6C == 0 & H1GI6D == 1 & H1GI6E == 0
replace RACE2 = 5 if H1GI6A == 0 & H1GI6B == 0 & H1GI6C == 0 & H1GI6D == 0 & H1GI6E == 1
replace RACE2 = 6 if RACE2 == . & H1GI6A < 2



/*
CONSTRUCTING AGE VARIABLE Wave 1

To compute a Wave I age variable with the Wave I data, use the following variables and formula:

IMONTH – Month interview completed
IDAY – Day interview completed
IYEAR – Year interview completed
H1GI1M – What is your birth date? month [and year]
H1GI1Y – What is your birth date? [month and] year

The respondent's age is constructed using the interview completion date and date of birth variables. Because only the month and year of birth are available, 15 is used as the day of birth when calculating age.
*/

* Construct a date of interview completion
generate IDATE = mdy(IMONTH, IDAY, IYEAR+1900)
format IDATE %d 

* Construct a date of birth
generate BDATE = mdy(H1GI1M, 15, H1GI1Y+1900)
format BDATE %d 

* Construct age
generate AGE = trunc((IDATE - BDATE)/365.25)


* CONSTRUCT SES/INCOME FOR WAVE 1
* Variable is PA55 TOTAL HOUSEHOLD INCOME
generate HOUSEHOLD_INCOME = PA55 * 1000


* MERGING WAVES TOGETHER
merge 1:1 AID using "add_health_wave_2_in_home.dta"
keep if _merge == 3
drop _merge
merge 1:1 AID using "add_health_wave_3_in_home.dta"
keep if _merge == 3
drop _merge
merge 1:1 AID using "add_health_wave_4_in_home.dta"
keep if _merge == 3
drop _merge
merge 1:1 AID using "add_health_wave_5_mixed.dta"
keep if _merge == 3


